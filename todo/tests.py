from django.test import TestCase
from datetime import datetime

from todo.models import Todo, TodoList


# Create your tests here.
class TodoTestCase(TestCase):

    TODO_TITLE = 'test element'

    def setUp(self):
        """
        La fonction setUp() est une méthode appelée avant l'exécution de chaque méthode de test
        """
        self.todoList = TodoList()
        self.todoList.name = 'test todo list'
        self.todoList.save()

        self.todoListTestElement = Todo()
        self.todoListTestElement.title = self.TODO_TITLE
        self.todoListTestElement.due_date = datetime.today()
        self.todoListTestElement.completed = True
        self.todoListTestElement.favorite = False
        self.todoListTestElement.list = self.todoList
        self.todoListTestElement.save()

    def test_create_todo(self):
        """
        On crée une nouvelle tâche et on vérifie que le nombre de tâches dans la base de données a augmenté d'un
        """
        nb_of_todos_before_add = Todo.objects.count()

        new_todo = Todo()
        new_todo.title = "Acheter du vin"
        new_todo.due_date = datetime.today()
        new_todo.completed = True
        new_todo.favorite = False
        new_todo.list = self.todoList

        new_todo.save()

        nb_of_todos_after_add = Todo.objects.count()

        self.assertTrue(nb_of_todos_after_add == nb_of_todos_before_add + 1)

    def test_update_todo(self):
        """
        Nous testons que le titre de l'élément de liste de tâches est égal au titre que nous lui avons défini dans la
        fonction setUp.

        Nous modifions ensuite le titre de l'élément de la liste de tâches,
        l'enregistrons, puis récupérons l'élément de la base de données.

        Nous testons ensuite que le titre de l'élément que nous avons obtenu de la base de données
         est égal au titre auquel nous l'avons modifié.
        """

        self.assertEqual(self.todoListTestElement.title, self.TODO_TITLE)

        self.todoListTestElement.title = 'Changed'
        self.todoListTestElement.save()

        tempElement = Todo.objects.get(pk=self.todoListTestElement.pk)

        self.assertEqual(tempElement.title, 'Changed')

    def test_delete_todo(self):
        """
        On teste que le nombre de tâches dans la base de données diminue d'un, lorsqu'une tâche est supprimée
        """

        nb_of_todos_before_delete = Todo.objects.count()
        self.todoListTestElement.delete()

        nb_of_todos_after_delete = Todo.objects.count()

        self.assertTrue(nb_of_todos_after_delete == nb_of_todos_before_delete - 1)
